let numeros = [1, 2, 3, 4, 5]
console.log('for in:\n')
for (let numero in numeros) {
    
    console.log(numero)
}
console.log('for of:\n')
for (let numero of numeros) {
    
    console.log(numero)
}
